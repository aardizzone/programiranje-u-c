﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vj1_zad1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("prvi broj: ");
            string input1 = Console.ReadLine();
            Console.WriteLine("drugi broj: ");
            string input2 = Console.ReadLine();

            int num1;
            int num2;
            int num3;

            try
            {//currency
                num1 = Int32.Parse(input1);
                num2 = Int32.Parse(input2);
                num3 = num1 / num2;
                Console.WriteLine(num3);
                Console.WriteLine("currency ");
                Console.WriteLine(num3.ToString("C2"));
            }
            catch (FormatException)
            {
                Console.WriteLine($"Unable to parse '{input1}'");
            }

            try
            {//Integer
                num1 = Int32.Parse(input1);
                num2 = Int32.Parse(input2);
                num3 = num1 / num2;
                Console.WriteLine("Integer ");
                Console.WriteLine(num3);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {//Scientific
                num1 = Int32.Parse(input1);
                num2 = Int32.Parse(input2);
                num3 = num1 / num2;
                Console.WriteLine("scientific ");
                Console.WriteLine(num3.ToString("E2"));
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            
            try
            {//Fixed-point
                num1 = Int32.Parse(input1);
                num2 = Int32.Parse(input2);
                num3 = num1 / num2;
                Console.WriteLine("Fixed point ");
                Console.WriteLine(num3.ToString("F2"));
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {//Number
                num1 = Int32.Parse(input1);
                num2 = Int32.Parse(input2);
                num3 = num1 / num2;
                Console.WriteLine("Number ");
                Console.WriteLine(num3.ToString("N2"));
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {//Hexadecimal
                num1 = Int32.Parse(input1);
                num2 = Int32.Parse(input2);
                num3 = num1 / num2;
                Console.WriteLine("Hexa ");
                Console.WriteLine(num3.ToString("X"));
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
    }

